RomanTable = {
  I: 1,
  V: 5,
  X: 10,
  L: 50,
  C: 100,
  D: 500,
  M: 1000,
};

RomanConverter = (output = 0) => {
  const input = String(
    document.getElementById("roman_numeral").value
  ).toUpperCase();

  lastDigit = String(input)[0];

  [...input].forEach((currentDigit) => {
    if (RomanTable[lastDigit] < RomanTable[currentDigit]) {
      output = Math.abs(output - parseInt(RomanTable[currentDigit]));
    } else {
      output += parseInt(RomanTable[currentDigit]);
    }
    lastDigit = currentDigit;
  });

  document.getElementById("result").innerHTML = output;
};
